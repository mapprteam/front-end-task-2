# Mappr Front End Task - 2

A Reactjs wrapper over Giphy API for real GIF masters.

## Project

We would like you to build a react component library that provides possibilities to get the trending GIFs on [Giphy](https://giphy.com/). Developers should be able to install the library into their applications as an NPM package and use the components provided by it. 
The library itself should not render any visual interfce. It should only provide data that can be displayed in any way the application developer desires.

- The library user should be able to get the list of the trending GIFs with pagination.
- The library user should be able to get the details (username, rating, anything you think could be relevant) of a certain GIF by ID.
- The library should include a demo page that showcases all the components provided by the library.

This project is pretty open ended in order to leave you some freedom to improve upon the base by focusing on what you like the most.

We strongly encourage you to pick a couple of optional improvements. The following are just some ideas, so if none of these interest you, feel free to do something that isn’t on this list:

- The library user should be able to provide his own [Giphy](https://giphy.com/) API key to the library instead of a hardcoded one
- The library user should be able to search for GIFs and get the list matching the search criteria
- The library user should be able to sort the results by uploaded time and specify if they prefer ascending or descending ordering
- The library user should be able to implement new GIF upload functionality through the library
- Add some unit tests



### Evaluation

The evaluator should be able to run `npm install YOUR-PACKAGE-NAME` from inside any existing React applicatin folder and be able to use the componetns inside the application. It should also be possible to browse the installed package folder and open the included **demo.html** file to view the component demonstration.
We’ll evaluate the exercise by looking at the end result and the code.

### Notes

Please, don't open a PR against this repo. Just follow the direction from the recruiter on how to submit the exercise.

### Coding at Mappr

At [Mappr](https://www.mappr.fr) we strive for writing simple, maintainable and clean code.

We prefer simplicity over complexity.

We comment our code and commit often.

We love our users and we really care about providing a good user experience and pleasant UI.

We encourage out of the box thinking and we love to be impressed!